package cowell.com.vn;

import javax.swing.JOptionPane;

import com.taskadapter.redmineapi.IssueManager;
import com.taskadapter.redmineapi.ProjectManager;
import com.taskadapter.redmineapi.RedmineException;
import com.taskadapter.redmineapi.RedmineManager;
import com.taskadapter.redmineapi.RedmineManagerFactory;
import com.taskadapter.redmineapi.bean.CustomField;
import com.taskadapter.redmineapi.bean.Issue;

public class Redmine {
	public static void UpdateRedmineWithApiKey(String url, String apiAccessKey, String projectName, String ticketID, String addcount, String delcount  ) throws RedmineException{
		//RedmineManager mgr = RedmineManagerFactory.createWithApiKey(url, apiAccessKey);
		RedmineManager mgr = RedmineManagerFactory.createWithUserAuth(url, "phucdcv", "123456789");
		IssueManager issueManager = mgr.getIssueManager();
		ProjectManager pm = mgr.getProjectManager();
		for (int i = 0; i < pm.getProjects().size(); i++ ){
			if (projectName.equals(pm.getProjects().get(i).getName().toString()) ){
		   		for (int j = 0; j < issueManager.getIssues(pm.getProjects().get(i).getIdentifier(),null ).size(); j ++){
		   			if (Integer.parseInt(ticketID)  == issueManager.getIssues(pm.getProjects().get(i).getIdentifier(),null ).get(j).getId()){
		   				Issue ticket = issueManager.getIssues(pm.getProjects().get(i).getIdentifier(),null ).get(j);
		   				CustomField addcountField = ticket.getCustomFieldById(1);
		   				CustomField delcountField = ticket.getCustomFieldById(2);
		   				addcountField.setValue(addcount);
		   				delcountField.setValue(delcount);
		   				ticket.clearCustomFields();
		   				ticket.addCustomField(addcountField);
		   				ticket.addCustomField(delcountField);
		   				issueManager.update(ticket);
		   				System.out.println("Updated Redmine");
		   			}
		   		}
			}
		 }
	}
}
