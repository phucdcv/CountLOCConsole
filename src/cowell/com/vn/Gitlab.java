package cowell.com.vn;

import static org.junit.Assume.assumeNoException;

import java.io.IOException;
import java.net.ConnectException;
import java.sql.Date;
import java.util.List;

import org.gitlab.api.GitlabAPI;
import org.gitlab.api.GitlabAPIException;
import org.gitlab.api.models.GitlabCommit;
import org.gitlab.api.models.GitlabCommitComparison;
import org.gitlab.api.models.GitlabProject;
import org.gitlab.api.models.GitlabUser;

public class Gitlab {
	public static GitlabAPI api;
	public static void setup(String username, String password, String URL, String Token) throws IOException {
	     
        api = GitlabAPI.connect(URL, Token);
        try {
            api.dispatch().with("login", username).with("password", password).to("session", GitlabUser.class);
           
        } catch (ConnectException e) {
            assumeNoException("GITLAB not running on '" + URL + "', skipping...", e);
        } catch (GitlabAPIException e) {
            final String message = e.getMessage();
            if (!message.equals("{\"message\":\"401 Unauthorized\"}")) {
                throw new AssertionError("Expected an unauthorized message", e);
            } else if(e.getResponseCode() != 401) {
                throw new AssertionError("Expected 401 code", e);
            }
        }
    }
	private static int GetProjectID(String ProjectName) throws IOException {
		List<GitlabProject> gitlabProjects = api.getProjects();
		int projID = 0;
		for (int i=0; i < gitlabProjects.size(); i++ )
		{
			if (ProjectName.equals(gitlabProjects.get(i).getName())){
				projID = gitlabProjects.get(i).getId();
			}
		}
		
		return projID;
	}
	private static GitlabCommit GetLastCommit (int projId, StringBuilder BranchName) throws IOException{
		Date lastcomitdate = new java.sql.Date(api.getLastCommits(projId,api.getBranches(projId).get(0).getName()).get(0).getCreatedAt().getTime());
		String commitHash =api.getLastCommits(projId,api.getBranches(projId).get(0).getName()).get(0).getId();
		BranchName.append(api.getBranches(projId).get(0).getName());
		for (int i = 1; i < api.getBranches(projId).size(); i++){
			Date temp = new java.sql.Date(api.getLastCommits(projId,api.getBranches(projId).get(i).getName()).get(0).getCreatedAt().getTime());
			if (lastcomitdate.before(temp))
			{
				BranchName.append(api.getBranches(projId).get(i).getName()) ;
				lastcomitdate = temp;
				commitHash = api.getLastCommits(projId,api.getBranches(projId).get(i).getName()).get(0).getId();
			}
		}
		 		
		return api.getCommit(projId, commitHash);
	}
	
	public static void GetCountLOC(String ProjectName, StringBuilder ticketid , StringBuilder add , StringBuilder del) throws IOException{

		int projId = GetProjectID(ProjectName);
		StringBuilder branchName = new StringBuilder();
		GitlabCommit lastcomit = GetLastCommit(projId, branchName);
		ticketid.append(lastcomit.getMessage().split("_")[0]);
		String BranchFrom = branchName + "_" + ticketid;
		String BranchTo = branchName.toString();
		CompareCommits(projId, BranchFrom, BranchTo, add, del);
	}
	
	private static void CompareCommits(int ProjectID, String BranchFrom, String BranchTo, StringBuilder add ,StringBuilder del) throws IOException {
		GitlabCommitComparison compare = api.compareCommits(ProjectID, BranchFrom, BranchTo);
		int countadd = 0;
		int countdel = 0;
		for (int i = 0; i < compare.getDiffs().size(); i++){
			if (countcode(compare.getDiffs().get(i).getDiff().toString(), "\n+") != 0)
			{
				countadd = countadd + countcode(compare.getDiffs().get(i).getDiff().toString(), "\n+") - 1;
			}
			
			countdel = countdel + countcode(compare.getDiffs().get(i).getDiff().toString(), "\n-");
		
		}
		
		add.append(String.valueOf(countadd));
		del.append(String.valueOf(countdel));
	}
	
	private static int countcode(String diff, String ch) {
        int words = 0;
        int fromIndex = diff.indexOf(ch, 0);
        while (fromIndex >= 0) {
            ++words;
            fromIndex = diff.indexOf(ch, fromIndex + 1);
        }
        
        return words;
    }

}
